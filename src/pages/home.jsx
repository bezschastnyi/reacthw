import {Component} from "react";
import Button from "../components/button";
import Modal from "../components/modal";
import styles from "./home.scss"

export class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpenModal1: false,
            isOpenModal2: false
        }
    }

    render() {
        return (
            <>
                <div className={'container'}>
                <Button
                    text="Open first modal"
                    background={"green"}
                    onClick={() =>
                        this.setState(() => ({
                            isOpenModal1: true,
                        }))
                    }
                />

                <Button
                    text="Open second modal"
                    background={"red"}
                    onClick={() =>
                        this.setState(() => ({
                            isOpenModal2: true,
                        }))
                    }
                />

                {this.state.isOpenModal1 ? <Modal
                    header="Do you want to delete this file?"
                    closeButton={true}
                    close={() =>
                        this.setState(() => ({
                        isOpenModal1: false,
                    }))}
                    onClick={() =>
                        this.setState(() => ({
                            isOpenModal1: true,
                        }))
                    }
                    text="Once you delete this file, it won’t be possible to undo this action.
                Are you sure you want to delete it?"/> : null}


                {this.state.isOpenModal2 ? <Modal
                    header="Lorem ipsum dolor sit amet!"
                    closeButton={true}
                    close={() =>
                        this.setState(() => ({
                            isOpenModal2: false,
                        }))}
                    onClick={() =>
                        this.setState(() => ({
                            isOpenModal2: true,
                        }))
                    }
                    text="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.?"/> : null}
                </div>
            </>
        )
    }
}