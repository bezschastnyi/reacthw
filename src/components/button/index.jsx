import {Component} from "react";
import style from "./button.scss"

export default class Button extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <>
            <button className={'btnPrimary'}
                style={{background: this.props.background}}
                onClick={this.props.onClick}
            >
                {this.props.text}
            </button>
            </>
        )
    }
}