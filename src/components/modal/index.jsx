import {Component} from "react";
import styles from "./modal.scss"
import Button from "../button";

export default class Modal extends Component{
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <>
                <div onClick={this.props.close} className={'containerModal'}>
                <div onClick={e=>e.stopPropagation()} className={'headerModal'} style={{background:this.props.bgColor}}>
                    <p className='headerText'>{this.props.header}</p><div className={'closeButton'} onClick={this.props.close}>X</div>

                </div>
                    <div onClick={e=>e.stopPropagation()} className={'bodyModal'} style={{background:this.props.bgColor}}>
                        <p className='bodyText'>{this.props.text}</p>
                        <div className={'btnModal'}>
                            <div className={'btn'}>OK</div>
                            <div className={'btn'}>CANCEL</div>
                        </div>
                    </div>
                </div>
            </>
        )
    }

}

